SHELL := /bin/bash

VERSION=`git describe --exact-match --tags HEAD 2> /dev/null`
COMMIT=`git rev-parse HEAD 2> /dev/null`
DATE_BUILD=`date +%Y-%m-%d\_%H:%M`

GOBIN = $(GOPATH)/bin
GOLINT = $(GOBIN)/golint

.PHONY: all lint test cover build clean help fmt

all: build

$(GOLINT):
	go get -u golang.org/x/lint/golint

fmt: ## Make gofmt persistant
	gofmt -s -w -l src/

lint: $(GOLINT) ## Start lint
	diff -u <(echo -n) <(gofmt -s -d src/); [ $$? -eq 0 ]
	go vet ./src/...
	diff -u <(echo -n) <(golint src/...); [ $$? -eq 0 ]

test: ## Run test
	go test -race -v -coverprofile=coverage.txt ./src/...

cover: test ## Display test coverage percent
	go tool cover -func coverage.txt

build: ## Build debug binary
	CC=arm-linux-gnueabi-gcc CGO_ENABLED=1 GOOS=linux GOARCH=arm GOARM=7 go build -ldflags "-X main.version=debug" -o "build/dht22" src/*.go

release: ## Build release binary
	CC=arm-linux-gnueabi-gcc CGO_ENABLED=1 GOOS=linux GOARCH=arm GOARM=7 go build -ldflags "-X main.version=${VERSION} -X main.commit=${COMMIT} -X main.date=${DATE_BUILD}" -o "build/dht22" src/*.go

clean: ## Remove vendors and build
	rm -rf build

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
