package main

type TempData struct {
	Temperature float32
	Humidity    float32
	Retries     int
	Error       string
}

func rawDataToStruct(temperature, humidity float32, retries int, error error) TempData {
	if error == nil {
		return TempData{
			Temperature: temperature,
			Humidity:    humidity,
			Retries:     retries,
			Error:       "",
		}
	} else {
		return TempData{Error: error.Error()}
	}
}
