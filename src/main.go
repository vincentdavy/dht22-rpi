package main

import (
	"encoding/json"
	"fmt"
	"github.com/d2r2/go-dht"
	"github.com/d2r2/go-logger"
	"os"
)

const (
	sensorType = dht.DHT22
	pin        = 4
	retry      = 10
)

func main() {
	_ = logger.ChangePackageLogLevel("dht", logger.FatalLevel) //disable logging
	tempData := rawDataToStruct(dht.ReadDHTxxWithRetry(sensorType, pin, false, retry))

	json, err := json.Marshal(tempData)
	if err != nil {
		_, _ = fmt.Fprint(os.Stderr, err)
		os.Exit(1)
	}

	fmt.Printf(string(json))
}
