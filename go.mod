module dht22

go 1.12

require (
	github.com/d2r2/go-dht v0.0.0-20190501193753-b6103ae97a4b
	github.com/d2r2/go-logger v0.0.0-20181221090742-9998a510495e
	github.com/d2r2/go-shell v0.0.0-20190508080434-6fd313082bbf // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
)
