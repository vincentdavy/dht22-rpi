FROM golang

RUN apt update && apt install -y gcc-arm-linux-gnueabi build-essential

ENV GO111MODULE=on

WORKDIR /go/src